function triggerBot() {
    fetch('https://api-cms.digitalcx.com/event/Vive_Welkom?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl')
    .then(response => { response.json().then(
        function(data) {
            let sessionId = data.sessionId;
            let eventResponse = data.outputs[0].outputParts[0];
            let responseStrings = eventResponse.text.split(/\r?\n/);
            let dialogPath = data.outputs[0].dialogPath;
            let htmlObject = '';
            for (var i = 0; i < responseStrings.length; i++) {
                let string = responseStrings[i];
                if (string !== "  " && string !== "" && !string.includes('DialogOption')) {
                    let htmlString = '<p>' + string + '</p>';
                    htmlObject = htmlObject.concat(htmlString);
                    eventResponse.text = htmlObject;
                }
            }
            localStorage.setItem("sessionid", sessionId);
            localStorage.setItem("bot", 'Vive');
            let hasOptions = '';
            let dialogOptions;
            if (eventResponse.userAnswer) {
                userAnswer = 'userAnswer';
            }
            if (eventResponse.dialogOptions) {
                if (eventResponse.dialogOptions.length) {
                    hasOptions = 'options';
                }
            }

            let optionsHtml = '';
            if (eventResponse.dialogOptions) {
                if (eventResponse.dialogOptions.length) {
                    for (var i = 0; i < eventResponse.dialogOptions.length; i++) {
                        let option = eventResponse.dialogOptions[i];
                        let dataOption = option.toLowerCase();
                        dataOption = dataOption.split(' ').join('_');
                        let optionHtml = '<span class="conv_option" data-option="' + dataOption + '" onclick="selectOption(\'' + option + '\', \'' + dialogPath + '\', this)">' + option + '</span>'
                        optionsHtml += optionHtml;
                    }
                }
            }
            let messageHtml =
                '<div class="conv_message last ' + hasOptions + '">' +
                '<div>' + eventResponse.text + '</div>' +
                '<p class="conv_options">' +
                    optionsHtml +
                '</p>'+
            '</div>';
            const botBody = document.getElementById('bot_body');
            botBody.innerHTML = '<div id="waiting-element">' +
                '<p>' +
                    '<img src="https://www.vvaa.nl/-/media/service/icons/typing.svg" />' +
                '</p>' +
            '</div>';
            let messages = document.querySelectorAll('.conv_message');
            messages.forEach(function(el) {
                el.classList.remove('last');
            })
            botBody.insertAdjacentHTML('afterbegin', messageHtml);
            let bot = document.getElementById('bot');
            bot.classList.add('active');
        });
    });
}

document.addEventListener("DOMContentLoaded", function() {
    let bot = document.getElementById('bot');
    let bot_wrap = document.getElementById('bot_wrap');
    bot_wrap.style.display = 'block';
    let botHead = document.getElementById('bot_header');
    let botHeadH3 = botHead.querySelector('h3');
    let botHeadH4 = botHead.querySelector('h4');
    botHead.addEventListener("click", function() {
        if (bot.classList.contains('active-open')) {
            bot.classList.remove('active-open');
            bot.classList.add('active');
            bot.classList.remove('semi-active');
        }
        else {
            triggerBot();
            bot.classList.add('pre-launch');
            setTimeout(function() {
                bot.classList.remove('pre-launch');
                bot.classList.add('active-open');
                bot.classList.remove('active');
                bot.classList.remove('semi-active');
            }, 1000);
        }
    });
    bot.onmouseover = function(){
        bot.classList.add('semi-active');
    };
    bot_wrap.onmouseleave = function(){
        bot.classList.remove('semi-active');
    };
    setTimeout(function () {
        bot.classList.add('semi-active');
    }, 3000);
});

function selectOption(dialogOption, dialogpath, e) {
    let sessionId = localStorage.getItem("sessionid");
    let dataDialogOption = dialogOption.toLowerCase();
    dataDialogOption = dataDialogOption.split(' ').join('_');
    let dialogOptionItems = document.querySelectorAll('.conv_option');
    dialogOptionItems.forEach(function(el) {
        el.classList.add('noclick');
        if (el.dataset.option === dataDialogOption) {
            el.classList.add('active');
        }
    })
    let waitingElement = document.getElementById('waiting-element');
    waitingElement.style.display = 'block';
    let messageHtml =
        '<div class="conv_message userAnswer">' +
        '<div><p>' + dialogOption + '</p></div>' +
    '</div>';
    let items = document.querySelectorAll('.conv_message');
    let lastItem = items[items.length- 1];
    lastItem.insertAdjacentHTML('afterend', messageHtml);
    toBottom();
    dialogOption = encodeURI(dialogOption);
    let dialogPathPart = '';
    if (dialogpath) {
        dialogPathPart = '&session.dialogPath=' + dialogpath;
    }
    fetch('https://api-cms.digitalcx.com/ask?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&session.id='+sessionId+'&q='+dialogOption + '&dim.Kanaal=Chatbot_Service' + dialogPathPart)
    .then(response => { response.json().then(
        function(data) {
            let responseId = data.outputs[0].outputId;
            let eventResponse = data.outputs[0].outputParts[0];
            let interactionId = data.interactionId;
            let dialogPath = data.outputs[0].dialogPath;
            linksAndImagesVA(eventResponse, interactionId);
            let responseStrings = eventResponse.text.split(/\r?\n/);
            let htmlObject = '';
            let bubbleAmount;
            let cleanResponseStrings = [];
            let bubbles = [];
            for (var i = 0; i < responseStrings.length; i++) {
                let string = responseStrings[i];
                if (string !== "  " && string !== "" && !string.includes('DialogOption')) {
                    string = marked(string);
                    string = string.replace('<ul>', '');
                    string = string.replace('</ul>', '');
                    bubbleAmount = i;
                    cleanResponseStrings.push(string);
                }
            }
            for (var i = 0; i < cleanResponseStrings.length; i++) {
                let string = cleanResponseStrings[i];
                if (string.includes('<u>')) {
                    string = string.replace('<u>', '<u onclick="liveChat(\'VA\')">')
                }
                bubbles.push(string);
            }
            let optionsHtml = '';
            let hasOptions = '';
            if (eventResponse.dialogOptions) {
                if (eventResponse.dialogOptions.length) {
                    hasOptions = 'options';
                    for (var i = 0; i < eventResponse.dialogOptions.length; i++) {
                        let option = eventResponse.dialogOptions[i];
                        if (option.includes('contact')) {
                            hasOptions = 'options fullfilled'
                        }
                        let dataOption = option.toLowerCase();
                        dataOption = dataOption.split(' ').join('_');
                        let optionHtml = '<span class="conv_option" data-option="' + dataOption + '" onclick="selectOption(\'' + option + '\', \'' + dialogPath + '\', this)">' + option + '</span>'
                        optionsHtml += optionHtml;
                    }
                }
            }
            let optionsWrapperHtml =
                '<p class="conv_options">' +
                    optionsHtml +
                '</p>';
            let messageWrapperHtml =
                '<div class="conv_message last ' + hasOptions + '">' +
                    '<div>' + bubbles[0] + '</div>' +
                '</div>';
            let items = document.querySelectorAll('.conv_message');
            let lastItem = items[items.length- 1];
            setTimeout(function () {
                if (data.outputs[0].dialogPath && data.outputs[0].dialogPath.includes('!')) {
                    messageWrapperHtml =
                        '<div class="conv_message' + hasOptions + '">' +
                            '<div>' + bubbles[0] + '</div>' +
                        '</div>';
                }
                lastItem.insertAdjacentHTML('afterend', messageWrapperHtml);
                toBottom();
                let waitingElement = document.getElementById('waiting-element');
                if (bubbles.length === 1) {
                    waitingElement.style.display = 'none';
                    let items = document.querySelectorAll('.conv_message');
                    let lastItem = items[items.length- 1];
                    if (eventResponse.dialogOptions) {
                        if (eventResponse.dialogOptions.length) {
                            lastItem.insertAdjacentHTML('beforeend', optionsWrapperHtml);
                            toBottom();
                        }
                        if (data.outputs[0].dialogPath && data.outputs[0].dialogPath.includes('!') && !data.outputs[0].outputParts[0].text.includes('Als u nog vragen heeft') && !data.outputs[0].outputParts[0].text.includes('livechat')) {
                            if (!dialogOption.includes('geen%20lid') && !dialogOption.includes('andere%20vraag')) {
                                waitingElement.style.display = 'block';
                                setTimeout(function () {
                                    waitingElement.style.display = 'none';
                                    escalation();
                                }, 1000);
                            }
                            toBottom();
                        }
                        if (!data.outputs[0].dialogPath && data.outputs[0].interactionValue !== 'escalation_1x_noanswer') {
                            setTimeout(function () {
                                waitingElement.style.display = 'none';
                                escalation();
                                toBottom();
                            }, 1000);
                        }
                    }
                }
                else {
                    let innerBubbles = bubbles;
                    innerBubbles.shift();
                    setTimeout(function () {
                        for (var i = 0; i < innerBubbles.length; i++) {
                            let bubble = innerBubbles[i];
                            (function(index) {
                                setTimeout(function() {
                                    items = document.querySelectorAll('.conv_message');
                                    lastItem = items[items.length- 1];
                                    lastItem.insertAdjacentHTML('beforeend', bubble);
                                    if (innerBubbles.length - 1 === index) {
                                        items = document.querySelectorAll('.conv_message');
                                        lastItem = items[items.length-1];
                                        paragraphs = lastItem.querySelectorAll('p');
                                        lastParagraph = paragraphs[paragraphs.length-1];
                                        if (lastParagraph.innerHTML.includes('Bent u zo geholpen?')) {
                                            lastParagraph.innerHTML = '';
                                        }
                                        if (data.outputs[0].dialogPath && data.outputs[0].dialogPath.includes('!')) {
                                            if (!dialogOption.includes('geen%20lid') && !dialogOption.includes('andere%20vraag')) {
                                                waitingElement.style.display = 'block';
                                                setTimeout(function () {
                                                    waitingElement.style.display = 'none';
                                                    escalation();
                                                    toBottom();
                                                }, 1000);
                                            }
                                        }
                                        if (data.outputs[0].interactionValue === 'escalation_1x_noanswer') {
                                                waitingElement.style.display = 'none';
                                        }
                                        if (eventResponse.dialogOptions) {
                                            if (eventResponse.dialogOptions.length) {
                                                lastItem.insertAdjacentHTML('beforeend', optionsWrapperHtml);
                                                toBottom();
                                            }
                                        }
                                        waitingElement.style.display = 'none';
                                        toBottom();
                                    }
                                    toBottom();
                                }, i * 1000);
                                toBottom();
                            })(i);
                        }
                    }, 1000);
                }
            }, 1000);
        });
    });
}

function escalation() {
    fetch('https://api-cms.digitalcx.com/event/Vive_escalatie?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl')
    .then(response => { response.json().then(
        function(data) {
            let responseId = data.outputs[0].outputId;
            let eventResponse = data.outputs[0].outputParts[0];
            let interactionId = data.interactionId;
            let dialogPath = data.outputs[0].dialogPath;
            linksAndImagesVA(eventResponse, interactionId);
            let responseStrings = eventResponse.text.split(/\r?\n/);
            let htmlObject = '';
            let bubbleAmount;
            let cleanResponseStrings = [];
            let bubbles = [];
            for (var i = 0; i < responseStrings.length; i++) {
                let string = responseStrings[i];
                if (string !== "  " && string !== "" && !string.includes('DialogOption')) {
                    string = marked(string);
                    string = string.replace('<ul>', '');
                    string = string.replace('</ul>', '');
                    bubbleAmount = i;
                    cleanResponseStrings.push(string);
                }
            }
            for (var i = 0; i < cleanResponseStrings.length; i++) {
                let string = cleanResponseStrings[i];
                if (string.includes('<u>')) {
                    string = string.replace('<u>', '<u onclick="liveChat(\'VA\')">')
                }
                bubbles.push(string);
            }
            let optionsHtml = '';
            let hasOptions = '';
            if (eventResponse.dialogOptions) {
                if (eventResponse.dialogOptions.length) {
                    hasOptions = 'options';
                    for (var i = 0; i < eventResponse.dialogOptions.length; i++) {
                        let option = eventResponse.dialogOptions[i];
                        if (option.includes('contact')) {
                            hasOptions = 'options fullfilled'
                        }
                        let dataOption = option.toLowerCase();
                        dataOption = dataOption.split(' ').join('_');
                        let optionHtml = '<span class="conv_option" data-option="' + dataOption + '" onclick="selectOption(\'' + option + '\', \'' + dialogPath + '\', this)">' + option + '</span>'
                        optionsHtml += optionHtml;
                    }
                }
            }
            let optionsWrapperHtml =
                '<p class="conv_options">' +
                    optionsHtml +
                '</p>';
            let messageWrapperHtml =
                '<div class="conv_message last ' + hasOptions + '">' +
                    '<div>' + bubbles[0] + '</div>' +
                    optionsWrapperHtml +
                '</div>';
            let items = document.querySelectorAll('.conv_message');
            let lastItem = items[items.length- 1];
            lastItem.insertAdjacentHTML('beforeend', messageWrapperHtml);
            toBottom();
        });
    });
}

function positiveFeedbackVA(interactionId, question, answer) {
    let messageHtml =
        '<div class="conv_message userAnswer lastUserAnswer">' +
        '<div><p>' + answer + '</p></div>' +
    '</div>';
    let fulfillment = document.getElementById('fulfillment');
    fulfillment.insertAdjacentHTML('afterend', messageHtml);
    const data = {
        "originInteractionId": interactionId,
        "score": 1,
        "label": question,
    }
    fetch('https://api-cms.digitalcx.com/feedback?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(data)
    }).then((resp) => {return resp.text();
    }).then(function(data) {
        let thankYou = '<div class="conv_message final last">' +
                            '<div><p>Bedankt voor uw feedback!</p></div>' +
                        '</div>'
        let fulfillment = document.getElementById('fulfillment');
        let lastUserAnswer = document.querySelectorAll('.lastUserAnswer');
        let lastLastUserAnswer = lastUserAnswer[lastUserAnswer.length- 1];
        lastLastUserAnswer.insertAdjacentHTML('afterend', thankYou);
        toBottom();
        let conv_options = fulfillment.getElementsByClassName('conv_option');
        conv_options[0].classList.add('noclick');
        conv_options[1].classList.add('noclick');
    });
}

function negativeFeedbackVA(interactionId, question, answer) {
    let messageHtml =
        '<div class="conv_message userAnswer lastUserAnswer">' +
        '<div><p>' + answer + '</p></div>' +
    '</div>';
    let fulfillment = document.getElementById('fulfillment');
    fulfillment.insertAdjacentHTML('afterend', messageHtml);
    const data = {
        "originInteractionId": interactionId,
        "score": -1,
        "label": question,
    }
    fetch('https://api-cms.digitalcx.com/feedback?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(data)
    }).then((resp) => {return resp.text();
    }).then(function(data) {
        let botType = localStorage.getItem('bot');
        let chatHtml;
        if (botType === 'schade_melden_bot') {
            chatHtml = '<div class="conv_message final last">' +
                            '<div><p>Mijn collega helpt u graag telefonisch verder via <a href="tel:0302474886">030 247 48 86</a>.</div>' +
                        '</div>';
        }
        if (botType === 'opleidingen_bot') {
            chatHtml = '<div class="conv_message final last">' +
                            '<div><p>Mijn collega helpt u graag telefonisch verder via <a href="tel:0302474789">030 247 47 89</a>.</div>' +
                        '</div>';
        }
        else {
            chatHtml = '<div class="conv_message final last">' +
                            '<div><p>Mijn collega helpt u graag verder via de <u onclick="liveChat(\'' + 'VA' + '\')">chat</u>.</div>' +
                        '</div>';
        }
        let fulfillment = document.getElementById('fulfillment');
        let lastUserAnswer = document.querySelectorAll('.lastUserAnswer');
        let lastLastUserAnswer = lastUserAnswer[lastUserAnswer.length- 1];
        lastLastUserAnswer.insertAdjacentHTML('afterend', chatHtml);
        toBottom();
        let conv_options = fulfillment.getElementsByClassName('conv_option');
        conv_options[0].classList.add('noclick');
        conv_options[1].classList.add('noclick');
    });
}

// function revertChoice(dialogpath, el) {
//     el.classList.toggle('active');
//     let parentEl = el.closest('.conv_message');
//     let optionsHtml = el.nextSibling;
//     let options = optionsHtml.querySelectorAll('.conv-option');
//     options.forEach(function(el) {
//         el.classList.remove('active');
//     })
//     let sessionId = localStorage.getItem("sessionid");
//     fetch('https://VVAA-VVAA.digitalcx.com/dialogstep?path=' + dialogpath + '&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&session.id='+sessionId)
//     .then(response => { response.json().then(
//         function(data) {
//             console.log(data);
//         });
//     });
// }

function submitText(e) {
    e.preventDefault();
    let textInput = document.getElementById('textinput');
    let fulfillment = document.getElementById('fulfillment');
    if (fulfillment) {
        fulfillment.remove();
    }
    let inputValue = textInput.value;
    if (inputValue.length > 1) {
        selectOption(inputValue);
        textInput.value = '';
    }
}

// UTILS
function linksAndImagesVA(answer, interactionId) {
    if (answer.links.length) {
        for (var a = 0; a < answer.links.length; a++) {
            let iterator = a + 1;
            let link = answer.links[a];
            let url = link.url;
            let id = link.id;
            let linkHTML = '<a href="' + url + '" target="blank" data-id="' + link.id + '" data-interaction="' + interactionId + '" onclick="selectLink(this)">' + link.text + '</a>';
            let linkString = '%{Link(' + iterator + ')}';
            answer.text = answer.text.replace(linkString, linkHTML);
            if (answer.images.length) {
                for (var b = 0; b < answer.images.length; b++) {
                    let image = answer.images[b];
                    let iteratorImages = b + 1;
                    let imageHtml = '';
                    if (image.linkId) {
                        if (link.id === image.linkId) {
                            let imageHTML = '<a href="' + link.url + '" target="_blank"><img src="' + image.name + '" alt ="' + image.title + '"></img></a>';
                            let imageString = '%{Image(' + iteratorImages + ')}';
                            answer.text = answer.text.replace(imageString, imageHTML);
                        }
                    }
                    else {
                        let imageHTML = '<img src="' + image.name + '" alt ="' + image.title + '"></img>';
                        let imageString = '%{Image(' + iteratorImages + ')}';
                        answer.text = answer.text.replace(imageString, imageHTML);
                    }
                }
            }
        }
    }
}

function toBottom() {
    let bottom = document.getElementById("bot_body");
    bottom.scrollTop = bottom.scrollHeight;
}

function liveChat(type) {
    let bot = document.getElementById('bot');
    bot.classList.remove('active');
    bot.classList.remove('active-open');
    if (navigator.userAgent.toUpperCase().indexOf("TRIDENT/") != -1 ||
    navigator.userAgent.toUpperCase().indexOf("MSIE") != -1 ||
    navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
        alert('De live chat wordt niet ondersteund door uw internetbrowser. Probeer het opnieuw in een andere browser.')
    }
    else {
        if (type === 'tree') {
            let category = localStorage.getItem('category');
            let subcategory = localStorage.getItem('subcategory');
            let faq = localStorage.getItem('faq');
            embedded_svc.settings.extraPrechatFormDetails = [{
                label: "FAQ Category",
                value: category,
                displayToAgent: true,
            },
            {
                label: "FAQ Subject",
                value: subcategory,
                displayToAgent: true,
            },
            {
                label: "FAQ Question",
                value: faq,
                displayToAgent: true,
            }];
        }
        if (type === 'search') {
            let searchFaq = localStorage.getItem('faq');
            let term = localStorage.getItem('category');
            if (searchFaq) {
                embedded_svc.settings.extraPrechatFormDetails = [{
                    label: "FAQ Category",
                    value: 'Zoekbalk',
                    displayToAgent: true,
                },
                {
                    label: "FAQ Subject",
                    value: 'Suggestie',
                    displayToAgent: true,
                },
                {
                    label: "FAQ Question",
                    value: faq,
                    displayToAgent: true,
                }];
            }
            if (!searchFaq) {
                embedded_svc.settings.extraPrechatFormDetails = [{
                    label: "FAQ Category",
                    value: 'Zoekbalk',
                    displayToAgent: true,
                },
                {
                    label: "FAQ Subject",
                    value: 'Zoekterm',
                    displayToAgent: true,
                },
                {
                    label: "FAQ Question",
                    value: term,
                    displayToAgent: true,
                }];
            }

        }
        if (type === 'VA') {
            let category = localStorage.getItem('category');
            embedded_svc.settings.extraPrechatFormDetails = [
                {
                    label: "FAQ Category",
                    value: 'Chatbot',
                    displayToAgent: true,
                },
                {
                    label: "FAQ Subject",
                    value: 'Chatbot',
                    displayToAgent: true,
                },
                {
                    label:"VA Transcript",
                    value: category,
                    displayToAgent: true,
                }
            ];
        }
        embedded_svc.settings.extraPrechatInfo = [{
            entityFieldMaps: [{
                    doCreate: true,
                    doFind: false,
                    fieldName: "FAQCategory__c",
                    isExactMatch: true,
                    label: "FAQ Category"
                },
                {
                    doCreate: true,
                    doFind: false,
                    fieldName: "FAQSubject__c",
                    isExactMatch: true,
                    label: "FAQ Subject"
                },
                {
                    doCreate: true,
                    doFind: false,
                    fieldName: "FAQQuestion__c",
                    isExactMatch: true,
                    label: "FAQ Question"
                },
            ],
            entityName: "Case",
        }, ];
        embedded_svc.inviteAPI.inviteButton.acceptInvite();
    }
}
